# Experimentations

Here are some notes.
- [Selection of parameters](/sdms/doc/experiments/params)
- [Notes and choices for SDMS](/sdms/doc/experiments/notes)
  
Here is a set of experimentations.
- [HSVI](/sdms/doc/experiments/hsvi)
- [Q-Learning](/sdms/doc/experiments/qlearning)